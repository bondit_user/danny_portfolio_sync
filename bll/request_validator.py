import jsonschema
from connexion.decorators.validation import RequestBodyValidator

# from dal.attributes import dal_attributes, dal_custom_attributes
# from dal.attributes_agd import agd_dal_attributes
# from dal.views import dal_tbls, dal_views


'''
@jsonschema.draft4_format_checker.checks('attribute')
def is_attribute(val):
    return val in dal_attributes.keys() or val in dal_custom_attributes.keys() or agd_dal_attributes.keys()


@jsonschema.draft4_format_checker.checks('lookup')
def is_lookup(val):
    return val in dal_tbls.keys() or val in dal_custom_attributes.keys()


@jsonschema.draft4_format_checker.checks('view')
def is_view(val):
    return val in dal_views.keys()


def extend_data_service(validator_class):

    def check_x_oneof(validator, x_oneof, instance, schema):
        keys = {str(x) for x in instance if x in x_oneof}
        if len(keys) > 1:
            yield jsonschema.ValidationError("mutually exclusive: {}".format(tuple(keys)))

    return jsonschema.validators.extend(
        validator_class, {'x-oneof': check_x_oneof})


DataServiceDraft4Validator = extend_data_service(jsonschema.Draft4Validator)


class DataServiceRequestBodyValidator(RequestBodyValidator):
    def __init__(self, *args, **kwargs):
        super(DataServiceRequestBodyValidator, self).__init__(
            *args, validator=DataServiceDraft4Validator, **kwargs)

'''


class Validator(RequestBodyValidator):

    def __init__(self, schema, consumes, api, is_null_value_valid=False, validator=None):
        super().__init__(schema, consumes, api, is_null_value_valid, validator=self.validate)

    def validate(self, *args, **kwargs):
        pass


def extend_data_service(validator_class):

    def check_x_oneof(validator, x_oneof, instance, schema):
        keys = {str(x) for x in instance if x in x_oneof}
        if len(keys) > 1:
            yield jsonschema.ValidationError("mutually exclusive: {}".format(tuple(keys)))

    return jsonschema.validators.extend(
        validator_class, {'x-oneof': check_x_oneof})


#PortfolioRequestBodyValidator = Validator
PortfolioRequestBodyValidator = RequestBodyValidator
