import logging
from datetime import datetime
from flask import jsonify, url_for

from dal.portfolio import DalPortfolio
from errors import ForbiddenError, LivePortfolioAlreadyExists, PortfolioNotExists, PortfolioIdNone, PortfolioDeleted

logger = logging.getLogger(__name__)

response_code = {
    'SUCCESS': 200,
    'ERROR': 400
}


def create_portfolio(portfolio_data):
    assets = portfolio_data.pop('assets', [])
    cash = portfolio_data.pop('cash', [])
    portfolio = DalPortfolio()
    try:
        portfolio_id, status, errors = portfolio.create_portfolio(portfolio_data, assets, cash)
        base_url = url_for('.portfolio_sync_api_create_portfolio', _external=True)
        url = '{}/{}'.format(base_url, portfolio_id)
        result = {'status': status, 'result': {'portfolio_id': portfolio_id, 'url': url}}
        code = response_code[status]
    except LivePortfolioAlreadyExists as e:
        message = e.args[0]
        result = {'status': 'ERROR', 'errors': [message]}
        code = response_code['ERROR']
    return jsonify(result), code


def get_portfolio(portfolio_id):
    try:
        portfolio = DalPortfolio(portfolio_id=portfolio_id)
        portfolio_data, assets, cash = portfolio.get_portfolio()
        result = strip_null_values(portfolio_data)
        result['assets'] = [reformat_asset(strip_null_values(a)) for a in assets]
        result['cash'] = [strip_null_values(c) for c in cash]
        code = response_code['SUCCESS']
    except PortfolioNotExists as e:
        message = e.args[0]
        result = {'status': 'ERROR', 'errors': [message]}
        code = response_code['ERROR']
    except PortfolioDeleted as e:
        message = e.args[0]
        result = {'status': 'ERROR', 'errors': [message]}
        code = response_code['ERROR']
    return jsonify(result), code


def update_portfolio(portfolio_id, portfolio_data):
    assets = portfolio_data.pop('assets', [])
    cash = portfolio_data.pop('cash', [])
    try:
        portfolio = DalPortfolio(portfolio_id=portfolio_id)
        portfolio_id, status, errors = portfolio.modify_portfolio(portfolio_data, assets, cash)
        base_url = url_for('.portfolio_sync_api_create_portfolio', _external=True)
        url = '{}/{}'.format(base_url, portfolio_id)
        result = {'status': status, 'result': {'portfolio_id': portfolio_id, 'url': url}}
        code = response_code[status]
    except PortfolioIdNone as e:
        message = e.args[0]
        result = {'status': 'ERROR', 'errors': [message]}
        code = response_code['ERROR']
    except PortfolioNotExists as e:
        message = e.args[0]
        result = {'status': 'ERROR', 'errors': [message]}
        code = response_code['ERROR']
    return jsonify(result), code


def delete_portfolio(portfolio_id):
    try:
        portfolio = DalPortfolio(portfolio_id=portfolio_id)
        portfolio.delete_portfolio()
        result = {'status': 'SUCCESS'}
        code = response_code['SUCCESS']
    except PortfolioIdNone as e:
        message = e.args[0]
        result = {'status': 'ERROR', 'errors': [message]}
        code = response_code['ERROR']
    except PortfolioNotExists as e:
        message = e.args[0]
        result = {'status': 'ERROR', 'errors': [message]}
        code = response_code['ERROR']
    return jsonify(result), code


def get_portfolios():
    try:
        portfolio = DalPortfolio()
        ls = portfolio.get_portfolio_list()
        return jsonify(ls), 200
    except ForbiddenError as e:
        logger.exception(e.message)
        # msg = e.message.get('detail', e.message)
        return jsonify({'error': e.message}), 403
    except Exception as e:
        logger.exception(e.message)
        # msg = e.message.get('detail', e.message)
        return jsonify({'error': e.message}), 400


def strip_null_values(portfolio_data):
    result = {}
    for k, v in portfolio_data.items():
        if v is not None:
            result[k] = v
    return result


def reformat_asset(asset):
    date = asset.pop('purchase_date', None)
    price = asset.pop('purchase_price', None)
    if date is not None and price is not None:
        asset['purchase_value'] = dict(date=date.strftime('%Y-%m-%d'), price=price)
    return asset
