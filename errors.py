
class ForbiddenError(Exception):
    error_name = 'Forbidden Error'

    def __init__(self, msg):
        super(ForbiddenError, self).__init__(msg)

    def __str__(self):
        return self.message

class LivePortfolioAlreadyExists(Exception):
    pass

class PortfolioIdNone(Exception):
    pass

class PortfolioNotExists(Exception):
    pass


class PortfolioDeleted(Exception):
    pass
