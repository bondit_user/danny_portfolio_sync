import logging
from pytoolbox.dao import database as db
from dao.portfolio_sync import Portfolio, Assets, Cash, PortfolioAttributes, VersionControl, TblPortfolioType, \
    TblPortfolioStatus
from dal.versioning import get_potfolio_version
from dao import tables_portfolio
from builtins import list
from datetime import datetime
from errors import LivePortfolioAlreadyExists, PortfolioNotExists, PortfolioIdNone, PortfolioDeleted
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

logger = logging.getLogger(__name__)
from sqlalchemy import inspect
import pandas as pd
from settings import DB_URL


class VersioningPersistence(object):
    def __init__(self, df_data_list, version_control_df, version_control_tables_df, entity_type, index_column,
                 process_date, vendor_id, version_control_entity_name='version_control',
                 version_control_table_name='version_control_tables'):
        self.df_data_list = df_data_list
        self.version_control_df = version_control_df
        self.version_control_tables_df = version_control_tables_df
        self.entity_type = entity_type
        self.version_control_entity_name = version_control_entity_name
        self.version_control_table_name = version_control_table_name
        self.index_column = index_column
        self.process_date = process_date
        self.vendor_id = vendor_id

    def entity_persistence(self, session, new_versions, update_version, tables_entity,
                           version_control_entity_name='version_control',
                           version_control_table_name='version_control_tables'):

        for table_data in self.df_data_list:
            tbl_df = table_data['df_data_all']
            # tbl_name = table_data['table_name']
            tbl_df.rename(columns={'id': 'id_field', 'class': 'class_field', 'type': 'type_field'}, inplace=True)
            if 'new_version_flag' in tbl_df:
                tbl_df.drop('new_version_flag', axis=1, inplace=True)
            # TODO generalize removal extra index
            tbl_df.reset_index(inplace=True)
            tbl_df.set_index([self.index_column], inplace=True)

        vc = {}
        vc['df_data_all'] = self.version_control_df
        vc['table_name'] = self.version_control_entity_name
        self.df_data_list.append(vc)

        vc = {}
        vc['df_data_all'] = self.version_control_tables_df
        vc['table_name'] = self.version_control_table_name
        self.df_data_list.append(vc)

        dfList = new_versions.to_records(index=True)[self.index_column].tolist()
        logger.info('Starting persistence versions:')

        for entity_id in dfList:
            # try:
            logger.debug(self.entity_type + ", entity #: " + str(entity_id))
            for table_data in self.df_data_list:
                tbl_name = table_data['table_name']
                tbl_df = table_data['df_data_all']
                tbl_df = tbl_df.astype(object)
                tbl_df = tbl_df.where((pd.notnull(tbl_df)), None)
                if tbl_name not in (version_control_entity_name, version_control_table_name):
                    tbl_df = tbl_df.drop(['version_type', 'ver_previous'], axis=1, errors='ignore')
                else:
                    tbl_df = tbl_df.drop_duplicates(keep='first')
                    tbl_df = tbl_df.drop(['vendor_id'], axis=1, errors='ignore')

                tbl_obj = tables_entity[tbl_name]
                list_row_data = []

                if entity_id in tbl_df.index.values:
                    list_row_data = tbl_df.loc[[entity_id]].reset_index().to_dict('records')

                for row_data in list_row_data:
                    tbl_row = tbl_obj(**row_data)
                    session.add(tbl_row)

                    # If update

                if entity_id in update_version.index and tbl_name in ['version_control_tables', 'version_control']:
                    self.update_version(update_version, entity_id, tbl_name, session, tbl_obj,
                                        version_control_entity_name,
                                        version_control_table_name)

    def update_version(self, update_version, entity_id, tbl_name, session, tbl_obj, version_control_entity_name,
                       version_control_table_name):
        curr_update_version = update_version.loc[[entity_id]]  # todo check len

        version64 = curr_update_version['version'].values[0]
        version = int(version64)

        version_type64 = curr_update_version['version_type'].values[0]
        version_type = int(version_type64)
        if tbl_name in [version_control_entity_name, version_control_table_name]:
            idx = 'staging_id'
        else:
            idx = self.index_column
        session.query(tbl_obj).filter(getattr(tbl_obj, idx) == entity_id,
                                      getattr(tbl_obj, 'version') == version).update(
            {getattr(tbl_obj, 'version_type'): version_type})


class DalTbls(dict):
    def __init__(self, tbl_cls, items=[]):
        """items can be a list of pair_lists or a dictionary"""
        dict.__init__(self, items)
        self.engine = db.get_engine(url=DB_URL)
        self.tbl_cls = tbl_cls
        mapper = inspect(self.tbl_cls)
        self.tbl_cols = []
        for column in mapper.attrs:
            self.tbl_cols.append(column.key)

    def __getitem__(self, key):
        try:
            super().__getitem__(key)
        except KeyError:
            with db.get_session(self.engine) as session:
                try:
                    name, = session.query(getattr(self.tbl_cls, self.tbl_cols[1])).filter(
                        getattr(self.tbl_cls, self.tbl_cols[0]) == key).one()  # test with key
                    self.__setitem__(key, name)
                except NoResultFound:
                    id, = session.query(getattr(self.tbl_cls, self.tbl_cols[0])).filter(
                        getattr(self.tbl_cls, self.tbl_cols[1]) == key).one()  # test with key
                    self.__setitem__(key, id)

        return self.get(key)
        # return self[key]

    def __setitem__(self, key, val):
        dict.__setitem__(self, key, val)
        dict.__setitem__(self, val, key)

    def __delitem__(self, key):
        dict.__delitem__(self, self[key])
        dict.__delitem__(self, key)


class DalPortfolio:
    REMOVE_COLUMNS = ['portfolio_id', 'version', 'insert_date', 'last_modified_date', 'staging_id',
                      '_sa_instance_state', 'position_id']

    def __init__(self, portfolio_id=None):
        self.portfolio_id = portfolio_id
        self.external_id = None
        self.engine = db.get_engine(url=DB_URL)
        self.process_date = datetime.now()
        self.default_status = 'SUCCESS'
        self.error_status = 'ERROR'

    def _format_output(self, results, select_columns=None):
        results_dict = [u.__dict__ for u in results]
        tbl_status = DalTbls(TblPortfolioStatus)
        tbl_type = DalTbls(TblPortfolioType)
        map_key = lambda key: 'portfolio_status' if key == 'status_id'  else (
            'portfolio_type' if key == 'type_id' else key)
        map_value = lambda key, value: tbl_status[value] if key == 'status_id'  else (
            tbl_type[value] if key == 'type_id' else value)
        if select_columns:
            list = [{map_key(key): map_value(key, value) for key, value in u.items() if key in select_columns} for u in
                    results_dict]
        else:
            list = [{map_key(key): map_value(key, value) for key, value in u.items() if key not in self.REMOVE_COLUMNS}
                    for u
                    in results_dict]

        return list

    def _get_portfolio(self):

        with db.get_session(self.engine) as session:

            status_id, = session.query(Portfolio.status_id).filter_by(portfolio_id=self.portfolio_id).one()
            external_id, type_id, status_id, owner_id, = session.query(Portfolio.external_id, Portfolio.type_id,
                                                                       Portfolio.status_id,
                                                                       Portfolio.owner_id).filter_by(
                portfolio_id=self.portfolio_id).one()

            tbl_lookup = DalTbls(TblPortfolioStatus)
            deleted_status_id = tbl_lookup['deleted']
            if status_id == deleted_status_id:
                raise PortfolioDeleted('Portfolio id {} already deleted.'.format(self.portfolio_id))

            tbl_lookup = DalTbls(TblPortfolioType)
            portfolio_type = tbl_lookup[type_id]

            version, = session.query(VersionControl.version).filter_by(staging_id=self.portfolio_id,
                                                                       version_type=2).one()

            port_att = session.query(PortfolioAttributes).filter_by(portfolio_id=self.portfolio_id,
                                                                    version=version).one()
            port_att_dic = self._format_output([port_att])[0]
            port_att_dic['portfolio_type'] = portfolio_type
            port_att_dic['external_id'] = external_id
            port_att_dic['owner_id'] = owner_id

            cash = session.query(Cash).filter_by(portfolio_id=self.portfolio_id, version=version).all()
            cash_list = self._format_output(cash)

            asset = session.query(Assets).filter_by(portfolio_id=self.portfolio_id, version=version).all()
            asset_list = self._format_output(asset)

        return port_att_dic, asset_list, cash_list

    def get_portfolio(self):
        if self.portfolio_id is None:
            raise Exception("no portfolio_id")
        try:
            return self._get_portfolio()
        except NoResultFound:
            raise PortfolioNotExists('Portfolio id {} not exists.'.format(self.portfolio_id))

    def get_portfolio_list(self, filters=None):
        with db.get_session(self.engine) as session:
            if filters is None:
                # todo join with version control for version_type=2
                results = session.query(Portfolio).all()
                portfolio_list = self._format_output(results=results, select_columns=['portfolio_id', 'status_id'])
            else:
                results = session.query(Portfolio).filter_by(**filters).all()
                portfolio_list = self._format_output(results=results, select_columns=['portfolio_id', 'status_id'])
        return portfolio_list

    def _extract_purchase(self, asset_dic):
        if 'purchase_value' in asset_dic:
            asset_dic['purchase_date'] = asset_dic['purchase_value']['date']
            asset_dic['purchase_price'] = asset_dic['purchase_value']['price']
            asset_dic.pop('purchase_value')
        return asset_dic

    def _insert_table(self, session, new_row, cls_tbl, auto_increment_key, **kwargs):
        key = None

        mapper = inspect(cls_tbl)
        for column in mapper.attrs:
            col_name = column.key
            value = None
            if col_name in kwargs:
                value = kwargs[col_name]
            if auto_increment_key is not None:
                if col_name != auto_increment_key and value is not None:
                    setattr(new_row, col_name, value)
            else:
                if value is not None:
                    setattr(new_row, col_name, value)
        session.add(new_row)
        session.flush()
        if auto_increment_key is not None:
            key = getattr(new_row, auto_increment_key)
        return key

    def _prepare_input_df(self, portfolio_attributes, assets_list, cash_list):
        entities_df = {}

        if any(portfolio_attributes.values()):
            portfolio_attributes_df = pd.DataFrame([portfolio_attributes],
                                                   columns=[column.key for column in inspect(PortfolioAttributes).attrs
                                                            if column.key != 'version'])
            portfolio_attributes_df["portfolio_id"] = self.portfolio_id
            entities_df['portfolio_attributes'] = portfolio_attributes_df

        if len(assets_list) > 0:
            assets_list = [self._extract_purchase(dict) for dict in assets_list]
            assets_list_df = pd.DataFrame(assets_list, columns=[column.key for column in inspect(Assets).attrs if
                                                                column.key != 'version'])
            assets_list_df["portfolio_id"] = self.portfolio_id
            # todo check if column position is not empty---->live
            if assets_list_df['position_id'].isnull().all():
                assets_list_df['position_id'] = assets_list_df.index + 1

            entities_df['assets'] = assets_list_df

        if len(cash_list) > 0:
            cash_list_df = pd.DataFrame(cash_list, columns=[column.key for column in inspect(Cash).attrs if
                                                            column.key != 'version'])
            cash_list_df['portfolio_id'] = self.portfolio_id
            entities_df['cash'] = cash_list_df

        return entities_df

    def _create_version(self, create_portfolio, portfolio_attributes, portfolio={}, assets_list=[], cash_list=[]):
        entity_list = []

        with db.get_session(self.engine) as session:
            if create_portfolio:
                self.portfolio_id = self._insert_table(session, Portfolio(), Portfolio, "portfolio_id", **portfolio)
                if "portfolio_creation_date" not in portfolio_attributes:
                    portfolio_attributes["portfolio_creation_date"] = self.process_date

            entity_list.append(self.portfolio_id)

            entities_df = self._prepare_input_df(portfolio_attributes, assets_list, cash_list)
            df_data_list, update_version, new_versions, version_control_df, version_control_tables_df, total_new_version, total_update_version = get_potfolio_version(
                session, self.engine, self.process_date, entity_list, entities_df)

            persistence_versions = VersioningPersistence(df_data_list, version_control_df, version_control_tables_df,
                                                         "portfolio",
                                                         index_column="portfolio_id",
                                                         process_date=self.process_date, vendor_id=0)

            if total_new_version > 0 or total_update_version > 0:
                persistence_versions.entity_persistence(session, new_versions, update_version, tables_portfolio)

            session.commit()

        return self.portfolio_id, self.default_status, None

    def _exist_portfolio_validation(self):
        filters = {}
        if self.portfolio_id is None:
            raise PortfolioIdNone('Portfolio id, is None, not defined for modification action.')

        filters["portfolio_id"] = self.portfolio_id
        portfolio_list = self.get_portfolio_list(filters)

        if len(portfolio_list) == 0:
            raise PortfolioNotExists('Portfolio id {} not exists. Can not be modified.'.format(self.portfolio_id))

    def delete_portfolio(self):

        self._exist_portfolio_validation()

        tbl_lookup = DalTbls(TblPortfolioStatus)
        deleted_status_id = tbl_lookup['deleted']

        with db.get_session(self.engine) as session:
            session.query(Portfolio).filter_by(portfolio_id=self.portfolio_id).update({'status_id': deleted_status_id})
            session.commit()

    def modify_portfolio(self, portfolio_attributes, assets_list=[], cash_list=[]):
        logger.debug(dict(portfolio=portfolio_attributes, assets=assets_list, cash=cash_list))

        self._exist_portfolio_validation()
        results = self._create_version(create_portfolio=False, portfolio_attributes=portfolio_attributes,
                                       assets_list=assets_list, cash_list=cash_list)

        return results

    def create_portfolio(self, portfolio_attributes, assets_list=[], cash_list=[]):
        logger.debug(dict(portfolio=portfolio_attributes, assets=assets_list, cash=cash_list))

        portfolio = {}
        external_id = portfolio_attributes.get("external_id", None)
        if external_id is not None:
            filters = {}
            filters["external_id"] = external_id
            portfolio["external_id"] = external_id
            portfolio_list = self.get_portfolio_list(filters)
            if len(portfolio_list) > 0:
                raise LivePortfolioAlreadyExists(
                    'Live portfolio {} already exists. Can not create again.'.format(external_id))

        portfolio["status_id"] = 1
        portfolio["owner_id"] = portfolio_attributes.get(
            'owner_id')  # todo if owner will be versioned, remove it from portfolio
        tbl_lookup = DalTbls(TblPortfolioType)
        portfolio_type = portfolio_attributes.pop('portfolio_type')
        if portfolio_type is not None:
            portfolio["type_id"] = tbl_lookup[portfolio_type]

        results = self._create_version(create_portfolio=True, portfolio=portfolio,
                                       portfolio_attributes=portfolio_attributes, assets_list=assets_list,
                                       cash_list=cash_list)

        return results


if __name__ == '__main__':
    print
    "========================================= etl programer use  ========================================="
    # input_list=InputList(vendor_id=40,data_process_id=555)
    p1 = Portfolio()
