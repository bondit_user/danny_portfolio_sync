import logging
from pytoolbox.dao import database as db
import pandas as pd

from dao.portfolio_sync import VersionControl, VersionControlTables
from pyetlutil.versioning import EntityVersionProccesor


def get_potfolio_version(session, engine, modify_date, entity_list, entities_df, entity_type='portfolio',
                         index_column='portfolio_id'):
    sql_query = session.query(VersionControl).filter(VersionControl.entity_type == entity_type,
                                                     VersionControl.staging_id.in_(entity_list))

    df_versions_entity = pd.read_sql(sql_query.statement, engine)

    sql_query = session.query(VersionControlTables).filter(
        VersionControlTables.staging_id.in_(entity_list))

    df_versions_tables = pd.read_sql(sql_query.statement, engine)

    excluded_cols = ['last_modified_date', 'position_id', 'insert_date', 'ver_previous']

    version_processor = EntityVersionProccesor(
        df_versions_entity=df_versions_entity,
        df_versions_tables=df_versions_tables,
        process_date=modify_date,
        vendor_id=0,
        index_column=index_column,
        entities_incremental_df=entities_df,
        column_list=excluded_cols,
        entity_type=entity_type
    )

    df_data_list, update_version, new_versions, version_control_df, version_control_tables_df = version_processor.multitable_version_entity()
    total_new_version = version_processor.entity_version_counter['total_new_version']
    total_update_version = version_processor.entity_version_counter['total_update_version']

    return df_data_list, update_version, new_versions, version_control_df, version_control_tables_df, total_new_version, total_update_version
