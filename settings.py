import os
import yaml
import logging.config
from pytoolbox.dao import database as db
from pytoolbox.load_config import read_config_file

MODULE_NAME = 'portfolio_sync'

CWD = os.path.dirname(os.path.abspath(__file__))

if 'SERVERTYPE' in os.environ and os.environ['SERVERTYPE'] == 'AWS Lambda':
    LOGGER_CONFIG = os.path.join(CWD, 'app_config', 'aws_logger.cnf')
else:
    # from pytoolbox.load_config import make_config_path
    # LOGGER_CONFIG = make_config_path('logger.cnf', MODULE_NAME)
    LOGGER_CONFIG = os.path.join(CWD, 'app_config', 'logger.cnf')  # TODO: fix logging config
logging.config.fileConfig(LOGGER_CONFIG, disable_existing_loggers=False)

CONNEXION_FILE = os.path.join(CWD, 'app_config', 'swagger.yaml')

DB_URL = os.environ.get('DB_CONNECTION_URL')
engine_portfolio = db.get_engine(url=DB_URL)

config_dict = read_config_file('portfolio_sync.yaml', config_type='yaml', module_name=MODULE_NAME)

mapping_rules = config_dict["mapping_rules"]
output_rules = config_dict["output_rules"]
swagger_def = yaml.load(open(CONNEXION_FILE))

portfolio_proposal_attributes = swagger_def['definitions']['PortfolioProposal']['properties']
portfolio_proposal_attributes.pop('assets')
portfolio_proposal_attributes.pop('cash')

api_definitions = {}
portfolio_attributes = swagger_def['definitions']['PortfolioLive']['properties']
portfolio_attributes.pop('assets')
portfolio_attributes.pop('cash')
api_definitions['portfolio_attributes'] = list(portfolio_attributes.keys())

assets_list = swagger_def['definitions']['Asset']['properties']
assets_list.pop('purchase_value')
api_definitions['assets_list'] = list(assets_list.keys())
purchase_price_attributes = swagger_def['definitions']['Price']['properties']
purchase_price_list = list(purchase_price_attributes.keys())
api_definitions['assets_list'] = api_definitions['assets_list'] + purchase_price_list

cash_list = swagger_def['definitions']['Cash']['properties']
api_definitions['cash_list'] = list(cash_list.keys())
