import logging
import connexion

from settings import CONNEXION_FILE, engine_portfolio
from bll.request_validator import PortfolioRequestBodyValidator
from bll import flow
from bondit_app.app import BondITApp

logger = logging.getLogger(__name__)

auth_tokens = {
    # user_id: (org_id, token)
    1: (1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzY29wZXMiOlsiY2lhbV9hZG1pbiIsIm5haXZlIl0sInVzZXJfaWQiOjEsImV4cCI6MTU1MzA3MDkxNSwib3JnX2lkIjoxfQ.A6YM8lh0IkjczmMHeN8MqHcHZT1nW9pt_ts10mVQAM4'),
    2: (2, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzY29wZXMiOlsiY2lhbV9hZG1pbiIsIm5haXZlIl0sInVzZXJfaWQiOjIsImV4cCI6MTU1MzA3MDkxNSwib3JnX2lkIjoyfQ.Rkk_Nfcbmw5hQcMwxr5ISOAQEPQ9aW-hJjfwS6vSfn8'),
    3: (2, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzY29wZXMiOlsiY2lhbV9hZG1pbiIsIm5haXZlIl0sInVzZXJfaWQiOjMsImV4cCI6MTU1MzA3MDkxNSwib3JnX2lkIjoyfQ._QlECur1M0I6L7XQJf3QP_TBjj16SvWdBJWKBlCJ4cA'),
}


def log_ip():
    logger.debug('Connect from {}'.format(connexion.request.environ['REMOTE_ADDR']))


def create_portfolio(portfolio_data):
    log_ip()
    logger.debug(portfolio_data)
    logger.debug('+ metadata: {}'.format(connexion.request.metadata))
    response, code = flow.create_portfolio(portfolio_data)
    return response, code


def get_portfolio(portfolio_id):
    log_ip()
    response, code = flow.get_portfolio(portfolio_id)
    return response, code


def update_portfolio(portfolio_id, portfolio_data):
    log_ip()
    logger.debug(portfolio_data)
    #logger.debug('+ metadata: {}'.format(connexion.request.metadata))
    response, code = flow.update_portfolio(portfolio_id, portfolio_data)
    return response, code


def delete_portfolio(portfolio_id):
    log_ip()
    response, code = flow.delete_portfolio(portfolio_id)
    return response, code


def get_portfolios():
    log_ip()
    response, code = flow.get_portfolios()
    return response, code


validator_map = {
    'body': PortfolioRequestBodyValidator
}
#validator_map = None

app = BondITApp(flask_app_name=__name__,
                       flask_config={'JSONIFY_PRETTYPRINT_REGULAR': False},
                       api_file_name=CONNEXION_FILE,
                       validator_map=validator_map,
                       strict_validation=True,
                       validate_responses=not True)
# app.app.register_error_handler(Exception, app.common_error_handler)

#app.api.strict_validation = False
#app.api.validate_responses = False

'''
http://10.0.125.151:8800/api/v1/ui/#!/default/login
user: admin, password: 82987546-5bfe-4c68-9fc7-ea590b04b948
'''

if __name__ == '__main__':
    logger.debug(engine_portfolio)
    app.run(port=5005)
