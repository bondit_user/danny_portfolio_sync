# portfolio_sync

REST web services to manage portfolios and related tasks

* Local URL:

    http://localhost:5005/api/v1/ui

## DB connection environment variable:

    DB_CONNECTION_URL=mysql://portfolio:portfolio@localhost/portfolio_sync?charset=utf8

## Docker configuration:

In a new version of docker, the `docker push` command (`make push`) gives an error.
To fix it:

* sudo vi /etc/docker/daemon.json -- add:

    , "insecure-registries": ["registry.bondit.co.il:5000"]

* sudo service docker restart
