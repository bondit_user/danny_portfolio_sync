import  dao.portfolio_sync
from sqlalchemy import inspect

tables_portfolio = dict([(getattr(cls, '__tablename__'), cls)
                   for _, cls in portfolio_sync.__dict__.items()
                   if isinstance(cls, type) and hasattr(cls, '__tablename__')
                   ])

columns_tables = dict([(getattr(cls, '__tablename__'), [column.key for column in inspect(cls).attrs])
                       for _, cls in portfolio_sync.__dict__.items()
                       if isinstance(cls, type) and hasattr(cls, '__tablename__')                    ])

pass