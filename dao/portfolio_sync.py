from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, DateTime, Date, BigInteger, SmallInteger, Integer, String, Float

Base = declarative_base()


class Assets(Base):
    __tablename__ = 'assets'

    portfolio_id = Column(BigInteger, primary_key=True, autoincrement=False)
    version = Column(Integer, primary_key=True, autoincrement=False)
    position_id = Column(Integer, primary_key=True, autoincrement=False)
    bondit_super_id = Column(BigInteger)
    nominal_value = Column(Integer)
    purchase_date = Column(DateTime)
    purchase_price = Column(Float(precision=22))
    currency_id = Column(Integer)
    insert_date = Column(DateTime)


class Cash(Base):
    __tablename__ = 'cash'

    portfolio_id = Column(BigInteger, primary_key=True, autoincrement=False)
    version = Column(Integer, primary_key=True, autoincrement=False)
    currency_id = Column(Integer, primary_key=True, autoincrement=False)
    accumulated_cash = Column(Float(precision=22))
    insert_date = Column(DateTime)


class Portfolio(Base):
    __tablename__ = 'portfolio'

    portfolio_id = Column(BigInteger, primary_key=True)
    type_id = Column(Integer)
    status_id = Column(Integer)
    owner_id = Column(BigInteger)
    external_id = Column(String(length=255))
    ttl = Column(Date)
    insert_date = Column(DateTime)


class PortfolioAttributes(Base):
    __tablename__ = 'portfolio_attributes'

    portfolio_id = Column(BigInteger, primary_key=True, autoincrement=False)
    version = Column(Integer, primary_key=True, autoincrement=False)
    owner_id = Column(BigInteger)
    portfolio_name = Column(String(length=255))
    portfolio_creation_date = Column(DateTime)
    portfolio_last_modified = Column(DateTime)
    currency_id = Column(Integer)
    insert_date = Column(DateTime)


class TblPortfolioStatus(Base):
    __tablename__ = 'tbl_portfolio_status'

    status_id = Column(Integer, primary_key=True)
    status_name = Column(String(length=255))


class TblPortfolioType(Base):
    __tablename__ = 'tbl_portfolio_type'

    type_id = Column(Integer, primary_key=True)
    type_name = Column(String(length=255))


class VersionControl(Base):
    __tablename__ = 'version_control'

    staging_id = Column(BigInteger, primary_key=True, autoincrement=False)
    entity_type = Column(String(length=40), primary_key=True, autoincrement=False)
    version = Column(Integer, primary_key=True, autoincrement=False)
    hash_id = Column(String(length=40))
    ver_previous = Column(Integer)
    version_type = Column(Integer)
    insert_date = Column(DateTime)
    last_modified_date = Column(DateTime)


class VersionControlTables(Base):
    __tablename__ = 'version_control_tables'

    staging_id = Column(Integer, primary_key=True, autoincrement=False)
    entity_type = Column(String(length=40), primary_key=True, autoincrement=False)
    source_instrument_id = Column(String(length=100))
    hash_id = Column(String(length=40))
    last_modified_date = Column(DateTime)
    version = Column(Integer, primary_key=True, autoincrement=False)
    ver_previous = Column(Integer)
    vendor_version = Column(Integer)
    version_type = Column(Integer)
    new_version_flag = Column(SmallInteger)
    feed_id = Column(Integer)
    obs_date = Column(Date)