.PHONY: build-python build-prod build-test run-test build-app push clean stop show-config

PORT = 5005
APP = portfolio_sync
DB_CONNECTION_URL = "mysql://portfolio:portfolio@10.0.125.27/portfolio_sync?charset=utf8"

HOST = pypi.bonditglobal.com:52.77.91.124
REGISTRY = 165071321881.dkr.ecr.eu-central-1.amazonaws.com
APP_TMP = tmpapp
REQUIREMENTS = $$(grep -v '\#' requirements.txt)

all: test

build-python:
	docker build . \
		--build-arg APP=$(APP) \
		--build-arg REQUIREMENTS="$(REQUIREMENTS)" \
		--add-host $(HOST) \
		--file docker/python/Dockerfile \
		--tag bondit/$(APP)-python:latest && \
	docker tag bondit/$(APP)-python:latest $(REGISTRY)/$(APP)-python:latest

build-prod: build-app build-python
	docker build . \
		--build-arg REGISTRY=$(REGISTRY) \
		--build-arg APP=$(APP) \
		--build-arg APP_TMP=$(APP_TMP) \
		--add-host $(HOST) \
		--file docker/python_prod/Dockerfile \
		--tag bondit/$(APP):latest
	echo "tag bondit/$(APP):latest"
	rm -rf $(APP_TMP)

build-app:
	rm -rf $(APP_TMP)
	mkdir -p $(APP_TMP)/tmpconf $(APP_TMP)/app_log/$(APP)
	cp -a app_config $(APP_TMP)/tmpconf/$(APP)
	cp -a docker/uwsgi.ini $(APP_TMP)/
	find * -maxdepth 0 -not \( -name venv -o -name $(APP_TMP) \) -print0 | \
		xargs -0 cp -a -t $(APP_TMP)

build-test: build-prod
	docker build . \
		--add-host $(HOST) \
		--file docker/python_test/Dockerfile \
		--tag bondit/$(APP)-test:latest

run-test: BUILD_TAG?=test_$(APP)_container
run-test:
	@echo + BUILD_TAG: $(BUILD_TAG)
	docker rm $(BUILD_TAG) || true
	docker run --name $(BUILD_TAG) \
		--env APP=$(APP) \
		bondit/$(APP)-test:latest test
run: BUILD_TAG?=$(APP)_container
run:
	@echo + BUILD_TAG: $(BUILD_TAG)
	docker rm $(BUILD_TAG) || true
	exec docker run --name $(BUILD_TAG) \
		--env DB_CONNECTION_URL="$(DB_CONNECTION_URL)" \
		--env LISTEN_PORT=$(PORT) \
		-p $(PORT):$(PORT) \
		bondit/$(APP):latest

run-bash: BUILD_TAG?=bash_$(APP)_container
run-bash:
	@echo + BUILD_TAG: $(BUILD_TAG)
	docker run -it --rm --entrypoint bash \
		--name $(BUILD_TAG) \
		--env APP=$(APP) \
		bondit/$(APP)-python:latest

post-test: BUILD_TAG?=test_$(APP)_container
post-test:
	@echo + BUILD_TAG: $(BUILD_TAG) && \
	docker cp $(BUILD_TAG):/app/report . && \
	docker rm $(BUILD_TAG)

stop:
	exec docker kill $(APP)_container

# helper targets

clean:
	find -name __pycache__ -o -name .cache -o -name .pytest_cache -print0 | xargs -0 rm -rf
	rm -rf $(APP_TMP)

push:
	docker tag bondit/$(APP):latest $(REGISTRY)/$(APP):latest && \
	docker push $(REGISTRY)/$(APP):latest

show-config:
	@echo APP: $(APP)
	@echo PORT: $(PORT)
	@echo DB_CONNECTION_URL: $(DB_CONNECTION_URL)

# targets to be executed inside test container

test-unit: clean
	export PYTHONPATH=.:./src:/app:$(PYTHONPATH) && \
	py.test --durations=10 -s -v -m "not integration" test \
		--cov algo --cov bll --cov database --cov kis --cov services --cov utility --cov views \
		--cov-report html:report --html=report/unit_tests.html --self-contained-html && \
	chmod -R 777 report

test: clean
	export PYTHONPATH=.:./src/gateway:/app/gateway:$(PYTHONPATH) && \
	py.test --durations=10 -s -v test \
		--cov service --cov toolkits --cov util \
		--cov-report html:report --html=report/unit_tests.html --self-contained-html && \
	chmod -R 777 report
test-custom: clean
	export PYTHONPATH=.:./src:./tests:$(PYTHONPATH) && \
	py.test -s -vv --durations=10 $(filter-out $@,$(MAKECMDGOALS))

%:
	@:
