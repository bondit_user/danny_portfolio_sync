import pytest
import yaml
import os
import logging.config
import pandas as pd
from dal.portfolio import DalPortfolio
from pytoolbox.dao import database as db
from dao import columns_tables, tables_portfolio
import datetime
import logging
from errors import LivePortfolioAlreadyExists
from settings import engine_portfolio, LOGGER_CONFIG
from dao import columns_tables, tables_portfolio

EXCLUDED_TABLES = ['tbl_portfolio_status', 'tbl_portfolio_type']
VERSIONING_TABLES = ['version_control', 'version_control_tables']
REMOVE_COLUMNS = ['portfolio_id', 'insert_date', 'last_modified_date', 'staging_id']

logging.config.fileConfig(LOGGER_CONFIG, disable_existing_loggers=False)
logger = logging.getLogger(__name__)


def clean_and_load_tables(db_actions):
    with db.get_session(engine_portfolio) as session:
        for table_name in tables_portfolio:
            if table_name not in EXCLUDED_TABLES and table_name[-4:] != '_bkp':
                sql_stmt = "TRUNCATE TABLE {}".format(table_name)
                session.execute(sql_stmt)
        for sql_action in db_actions:
            session.execute(sql_action['sql'])
        session.commit()


def get_table(table_name, portfolio_id):
    with db.get_session(engine_portfolio) as session:
        if table_name in VERSIONING_TABLES:
            if table_name == 'version_control':
                sql = session.query(tables_portfolio[table_name]).filter_by(staging_id=portfolio_id,
                                                                            entity_type='portfolio')
            else:
                sql = session.query(tables_portfolio[table_name]).filter(
                    getattr(tables_portfolio[table_name], 'staging_id') == portfolio_id,
                    getattr(tables_portfolio[table_name], 'entity_type').in_(
                        ['portfolio_attributes', 'assets', 'cash']))
        else:
            sql = session.query(tables_portfolio[table_name]).filter_by(portfolio_id=portfolio_id)
        df = pd.read_sql(sql.statement, engine_portfolio)
        df = df.astype(object)
        df = df.where((pd.notnull(df)), None)
        list_table = list(df.T.to_dict().values())
        for dict_t in list_table:
            for column_name in REMOVE_COLUMNS:
                if column_name in dict_t:
                    dict_t.pop(column_name)
    return list_table


def load_yaml_dir(dir_name):
    res = {}
    for file_name in os.listdir(dir_name):
        f_name, f_ext = os.path.splitext(file_name)
        if f_ext in ('.yaml', '.yml'):
            full_path = os.path.join(dir_name, file_name)
            res[f_name] = yaml.load(open(full_path))
        else:
            pass  # TODO: log warnnig?
    return res


CWD = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data_test_dal')
up_date = datetime.datetime.now()
process_date = up_date.date()

_tests = {}
_tests['tests'] = load_yaml_dir(CWD)
clean_and_load_tables(_tests['tests']['db_data_one_version_all_bonds']['sql_actions'])


@pytest.mark.parametrize('test', _tests['tests']['test_portfolio_put']['test_cases'])
def test_merge_config_dics(test):
    portfolio_dinamic_hash = False
    asset_list = test["input_"]["assets_list"]
    attributes = test["input_"]["portfolio_attributes"]
    cash = test["input_"]["cash_list"]
    portfolio_id = test["input_"]["portfolio_id"]
    test_name = test.get('name', None)
    dal_portfolio = DalPortfolio(portfolio_id=portfolio_id)
    logger.info('Running test: {}'.format(test_name))
    results = test["expected_result"]
    if test_name == 'portfolio_live_w_currency_same_ex_id':
        with pytest.raises(Exception) as e_info:
            portfolio_id, status, msg = dal_portfolio.modify_portfolio(attributes, asset_list, cash)
        assert str(e_info.value) == results['msg']
        assert e_info.typename == results['exeption_name']
    else:
        port_att_dic, assets_list, cash_list = dal_portfolio.get_portfolio()
        portfolio_id, status, msg = dal_portfolio.modify_portfolio(attributes, asset_list, cash)
        port_att_dic, assets_list, cash_list = dal_portfolio.get_portfolio()
        assert 1 == 1
        # for table_name in tables_portfolio:
        #     if table_name not in EXCLUDED_TABLES and table_name[-4:]!='_bkp':
        #         table_dic=get_table(table_name,portfolio_id)
        #         if table_name=='portfolio_attributes':
        #             if results['portfolio_attributes'][0]['portfolio_creation_date'] == 'now()':
        #                 results['portfolio_attributes'][0]['portfolio_creation_date'] = process_date
        #                 table_dic[0]['portfolio_creation_date']=table_dic[0]['portfolio_creation_date'].date()
        #                 portfolio_dinamic_hash = True
        #         if table_name == 'version_control' and portfolio_dinamic_hash:
        #             results['version_control'][0]['hash_id'] = table_dic[0]['hash_id']
        #         if table_name == 'version_control_tables' and portfolio_dinamic_hash:
        #             results['version_control_tables'][len(results[table_name])-1]['hash_id'] = table_dic[len(results[table_name])-1]['hash_id']
        #
        #         assert table_dic == results[table_name]
