
SET NAMES 'utf8';
USE portfolio_sync;

BEGIN;

--
-- Disable foreign keys
--
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

--
-- Inserting data into table tbl_portfolio_status
--
INSERT INTO tbl_portfolio_status(status_id, status_name) VALUES
(1, 'active'),
(2, 'expired'),
(3, 'deleted');

--
-- Inserting data into table tbl_portfolio_type
--
INSERT INTO tbl_portfolio_type(type_id, type_name) VALUES
(1, 'live'),
(2, 'proposal');

--
-- Enable foreign keys
--
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;

COMMIT;
